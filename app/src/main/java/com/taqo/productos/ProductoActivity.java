package com.taqo.productos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.taqo.productos.database.ProductosDB;
import com.taqo.productos.database.Producto;

public class ProductoActivity extends AppCompatActivity {
    
    private Button btnBuscar;
    private Button btnBorrar;
    private Button btnEditar;
    private Button btnCerrar;
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rbgPerecedero;
    private ProductosDB db;
    private Producto saveObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbgPerecedero = findViewById(R.id.rbgPerecedero);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnEditar = findViewById(R.id.btnEditar);
        btnCerrar = findViewById(R.id.btnCerrar);
        db = new ProductosDB(this);

        btnEditar.setOnClickListener(this::btnEditarAction);
        btnCerrar.setOnClickListener(v -> finish());
        btnBuscar.setOnClickListener(this::btnBuscarAction);
        btnBorrar.setOnClickListener(this::btnBorrarAction);
    }

    private void btnBorrarAction(View view)
    {
        db.openDatabase();
        db.deleteProducto(Long.parseLong(getText(txtCodigo)));
        Toast.makeText(this, "Producto Eliminado", Toast.LENGTH_LONG).show();
        db.close();
        limpiar();
    }

    private void btnBuscarAction(View view)
    {
        db.openDatabase();
        Producto producto = db.getProducto(Long.parseLong(getText(txtCodigo)));
        if(producto == null)
        {
            Toast.makeText(this, "Producto no Encontrado", Toast.LENGTH_LONG).show();
        }
        else
        {
            saveObject = producto;
            txtMarca.setText(saveObject.getMarca());
            txtNombre.setText(saveObject.getNombreProducto());
            txtPrecio.setText(String.valueOf(saveObject.getPreacio()));
            rbgPerecedero.check(saveObject.isPerecedero() ? R.id.rbPerecedero : R.id.rbNoPerecedero);
        }
        db.close();
    }



    private void btnEditarAction(View view) {
        db.openDatabase();
        if(validar())
        {
            Producto producto = new Producto();
            producto.setId(saveObject.getId());
            producto.setNombreProducto(getText(txtNombre));
            producto.setPreacio(Float.parseFloat(getText(txtPrecio)));
            producto.setMarca(getText(txtMarca));
            producto.setPerecedero(rbgPerecedero.getCheckedRadioButtonId() == R.id.rbPerecedero);
            if (db.updateProducto(producto) != -1)
                Toast.makeText(this, "Modficación realizada con exito", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Error en la modificación", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Porfavor llene todos los campos",
                    Toast.LENGTH_SHORT).show();

        }
        limpiar();
        db.close();
    }

    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        saveObject = null;

    }
    private boolean validar(){
        if(validarCampo(txtCodigo) || validarCampo(txtNombre) ||
                validarCampo(txtPrecio) || validarCampo(txtMarca))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }
}
